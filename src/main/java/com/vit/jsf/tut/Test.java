package com.vit.jsf.tut;

import javax.faces.bean.ManagedBean;

@ManagedBean(name = "test", eager = true)
public class Test {
   public Test() {
      System.out.println("HelloWorld started!");
   }
   public String getMessage() {
      return "Hello World!";
   }
}